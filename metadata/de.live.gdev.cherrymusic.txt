Categories:Multimedia,Internet
License:GPLv3
Web Site:https://github.com/de-live-gdev/cherrymusic_android/blob/HEAD/README.md
Source Code:https://github.com/de-live-gdev/cherrymusic_android
Issue Tracker:https://github.com/de-live-gdev/cherrymusic_android/issues
Donate:http://de-live-gdev.github.io/about/#donate
Bitcoin:1B9ZyYdQoY9BxMe9dRUEKaZbJWsbQqfXU5

Auto Name:Cherry
Summary:Wrapper for CherryMusic
Description:
Wrapper for CherryMusic, a self-hosted streaming web-application. A lightweight
alternative to Ampache, Google Play Music,Spotify,..

See [http://www.fomori.org/cherrymusic/] for more informations.
.

Repo Type:git
Repo:https://github.com/de-live-gdev/cherrymusic_android.git

Build:1.0,1
    commit=v1.0

Build:1.1,2
    commit=v1.1
    subdir=app
    gradle=yes

Build:1.11,4
    commit=v1.11
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.11
Current Version Code:4
